/* @params
 * selector: selector of element in which calendar is to be rendered
 * eventData: an array consisting of all titles, startTimes and EndTimes pertaining to an event in JSON format
 * single attr will look like {
        "startTime": "Sun May 24 2015 01:00:00 GMT+0530 (IST)",
        "endTime": "Sun May 24 2015 01:30:00 GMT+0530 (IST)",
        "title": "Event 1"
    },
 */
var calendar = ( function () {
	return function ( selector, eventData, options ) {
		//date object initialized with date which you want to display when application loads
		var today = new Date(),
		//counter which will keep track day when pressing next/previous
			currentDay = new Date(options.displayDate),
			parsedEvents = null,
		//it should be accessing only this calendar
			TIME_VALUE = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23],
			/*
			 *
			 */
			displayToDay = function () {
				if (currentDay === today) {
					return;
				}

				currentDay = new Date(today);
				//display today's events
				renderOverlays(sampleData, currentDay);
			},
			/*
			 *
			 */
			displayPreviousDay = function () {
				currentDay.setDate(currentDay.getDate() - 1); 
				//display today's events
				renderOverlays(sampleData, currentDay);
			},
			/*
			 * display and append the next day events.
			 */
			displayNextDay = function ()
			{
				currentDay.setDate(currentDay.getDate() + 1); 
				//display today's events
				renderOverlays(sampleData, currentDay);
			},
			getDiffDays = function ( date ) {
				return (date - new Date())/1000/60/60/24;
			},

			getDiffMonths = function ( date ) {
				return getDiffDays(date)/30;
			},

			getDiffYear = function ( date ) {
				return getDiffMonths(date)/12;
			},

			getDayIndex = function (date) {
				return new Date(date.setHours(0, 0, 0));
			},

			getParsedEvent = function ( event ) {
				return {
					startTime: new Date(event.startTime),
					endTime:   new Date(event.endTime),
					title:     event.title
				};
			},

			getParsedEventData = function ( events ) {
				var eventBuckets = {};

				events.forEach( function ( event ) {
					var parsedEvent = getParsedEvent( event ),
						date = new Date(parsedEvent.startTime),
						diffIndex = getDayIndex(date);

					eventBuckets[diffIndex] ? eventBuckets[diffIndex].push( parsedEvent ) : eventBuckets[diffIndex] = [parsedEvent];
				});

				return eventBuckets;
			},
			showCurrentDate = function(date) {
				//display date
				$('[data-display-date="calendar"]').html(date);
			},
			getHrStrIn12Format = function (hour) {
				return (hour === 12) ? ('12pm') : (hour < 12 ? (hour + 'am')  : (hour-12) + 'pm');
			},

			getTopFor = function(hour, minute) {
				return (hour)*60 + minute;
			},

			getHeightFor = function(startTime, endTime) {
				//here logic is get top of end time and starttime both and subtract it.
				var top_end = getTopFor(endTime.getHours(), endTime.getMinutes()),
					top_start = getTopFor(startTime.getHours(), startTime.getMinutes()),
					height_diff = top_end-top_start;
				//if it's negative then give 10px;
				if(height_diff < 0)
				{
					height_diff = 10;
				}
				return height_diff;
			},

			renderCalendar = function () {
				var $el = $(selector),
					overlayEl = '<div class="event-overlay"></div>',
					rows = '';

				TIME_VALUE.forEach( function ( time ) {
					var strFormat = getHrStrIn12Format(time);

					rows += ( '<tr>' +
								'<td id="' + strFormat + '" class="time-span-cell"><div class="time-span">' + strFormat + '</div></td>' +
								'<td class="event-cell"></td>' +
								'</tr>'
							);
				});

				$el.html( '<div class="calendar-container"><table class="event-table">' + rows + '</table>' + overlayEl + "</div>");

				//display today's events
				
			},

			renderOverlays = function ( events, date ) {
				var dayIndex = getDayIndex(date);
				//display date
				showCurrentDate(date);	
				//delete previous events
				deleteOverlays();
				//check if any events exists and render them if any
				parsedEvents[dayIndex] && parsedEvents[dayIndex].forEach( function ( event) {
					renderOverlay( event );
				} );
			},

			renderOverlay = function ( event ) {
				var startTime = event.startTime,
					startHrs = startTime.getHours(),
					startMinutes = startTime.getMinutes(),
					top = getTopFor(startHrs, startMinutes),
					height = getHeightFor(event.startTime, event.endTime),
					eventDiv = '<div class="event" style="top:'+ top + 'px;height:'+height+'">'+ event.title+ '</div>';
				//get that calendar's overlays div
				$(".calendar-container .event-overlay").append(eventDiv);
			},

			deleteOverlays = function() {
				$(".calendar-container .event-overlay").empty();
			},

			init = function () {
				$('[data-today="calendar"]').on('click', displayToDay);
				$('[data-previous="calendar"]').on('click', displayPreviousDay);
				$('[data-next="calendar"]').on('click', displayNextDay);
				//parse the json file to convert it to date objects rather then string
				parsedEvents = getParsedEventData( sampleData );
				renderCalendar();
				renderOverlays(sampleData, new Date(options.displayDate));
			};
			
			init();
	};
})();
